package com.ultimate.clevertap;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.QuickContactBadge;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clevertap.android.sdk.CleverTapAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    Button btnClickMe, btnUploadProfile, btnPushMyProfile;
    private RequestQueue mRequestQueue;

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? "CleverTap" : tag);
        getRequestQueue().add(req);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final CleverTapAPI clevertapDefaultInstance = CleverTapAPI.getDefaultInstance(getApplicationContext());
        setContentView(R.layout.activity_main);

        btnClickMe = findViewById(R.id.btnClickMe);

        btnClickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> prodViewedAction = new HashMap<String, Object>();
                prodViewedAction.put("Product ID", 1);
                prodViewedAction.put("Product Image", "https://d35fo82fjcw0y8.cloudfront.net/2018/07/26020307/customer-success-clevertap.jpg");
                prodViewedAction.put("Product Name", "Clever Tap");
                clevertapDefaultInstance.pushEvent("Product viewed", prodViewedAction);
                Toast.makeText(getApplicationContext(),"Product Viewed",Toast.LENGTH_SHORT).show();
            }
        });


        btnPushMyProfile = findViewById(R.id.btnPushMyProfile);
        btnPushMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> profile = new HashMap<String, Object>();
                profile.put("Email", "dkrajatg19@clevertap.com");
                clevertapDefaultInstance.pushProfile(profile);
                Toast.makeText(getApplicationContext(),"Product Viewed",Toast.LENGTH_SHORT).show();
            }
        });

        btnUploadProfile =findViewById(R.id.btnUploadProfile);
        btnUploadProfile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                UploadProfiles(ReadProfilesFromCSV());
            }
        });

    }

    private JSONObject ReadProfilesFromCSV() {
        String csvFile = "";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);

                System.out.println("Country [code= " + country[4] + " , name=" + country[5] + "]");

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //Convert into JSONObject

        return  new JSONObject();
    }

    public void UploadProfiles(final JSONObject obj)
    {
        final String TAG = "json_obj_req";
        String tag_json_obj = "upload_req";
        String url = "https://api.clevertap.com/1/upload";
         ;
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait while Profiles are uploading.");
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, obj,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();

                        Toast.makeText(getApplicationContext(),"Profiles uploaded Successfully",Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //showProgress(false);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                pDialog.hide();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=" + getParamsEncoding();
            }

            @Override
            protected String getParamsEncoding() {
                return "UTF-8";
            }

            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-CleverTap-Account-Id","RK4-R6K-565Z");
                params.put("X-CleverTap-Passcode", "EHA-IIB-UEKL");
                return params;
            }
        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

// Adding request to request queue
        addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
