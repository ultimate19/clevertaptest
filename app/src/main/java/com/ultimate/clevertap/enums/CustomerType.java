package com.ultimate.clevertap.enums;

public enum CustomerType {
    S,
    M,
    L,
    XL
}
